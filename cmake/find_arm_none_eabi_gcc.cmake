function(_arm_none_eabi_gcc_version)
    execute_process(
        COMMAND ${ARM_NONE_EABI_GCC_EXECUTABLE} --version
        OUTPUT_VARIABLE full_arm_none_eabi_gcc_version
        OUTPUT_STRIP_TRAILING_WHITESPACE
    )

    message(TRACE "arm-none-eabi-gcc version full: ${full_arm_none_eabi_gcc_version}")
    
    string(
        REGEX 
        MATCH "[0-9]+\.[0-9]+\.[0-9]+" 
        arm_none_eabi_gcc_version 
        "${full_arm_none_eabi_gcc_version}"
    )

    message(DEBUG "arm-none-eabi-gcc version: ${arm_none_eabi_gcc_version}")
    set(ARM_NONE_EABI_GCC_VERSION ${arm_none_eabi_gcc_version} PARENT_SCOPE)
endfunction()


function(find_arm_none_eabi_gcc VERSION)
    if(ARM_NONE_EABI_GCC_FOUND)
        return()
    endif()

    set(OPTIONS REQUIRED NO_SYSTEM_PATH)
    set(ONE_VALUE)
    set(MULTI_VALUE)
    cmake_parse_arguments(
        PARSE_ARGV 1 
        FIND_ARM_NONE_EABI_GCC 
        "${OPTIONS}" 
        "${ONE_VALUE}" 
        "${MULTI_VALUE}"
    )

    if(FIND_ARM_NONE_EABI_GCC_REQUIRED)
        set(REQUIRED "REQUIRED")
    endif()
    if(FIND_ARM_NONE_EABI_GCC_NO_SYSTEM_PATH)
        set(NO_SYSTEM_PATH "NO_SYSTEM_ENVIRONMENT_PATH")
    endif()

    find_program(
        ARM_NONE_EABI_GCC_EXECUTABLE 
        NAMES arm-none-eabi-gcc
        PATHS ${ARM_NONE_EABI_GCC_ROOT} ENV{ARM_NONE_EABI_GCC_ROOT} 
        PATH_SUFFIXES bin 
        ${REQUIRED} 
        ${NO_SYSTEM_PATH}
    )

    if(ARM_NONE_EABI_GCC_EXECUTABLE)
        _arm_none_eabi_gcc_version()

        if(NOT VERSION VERSION_EQUAL ARM_NONE_EABI_GCC_VERSION)
            if(NOT FIND_ARM_NONE_EABI_GCC_REQUIRED)
                message(VERBOSE 
                    "Found arm-none-eabi-gcc: ${ARM_NONE_EABI_GCC_EXECUTABLE} " 
                    "(found version \"${ARM_NONE_EABI_GCC_VERSION}\", required \"${VERSION}\")")
                unset(ARM_NONE_EABI_GCC_EXECUTABLE CACHE)
                set(ARM_NONE_EABI_GCC_FOUND FALSE PARENT_SCOPE)
                return()
            endif()
            message(
                FATAL_ERROR 
                "arm-none-eabi-gcc not found. "
                "(Required: ${VERSION}, found: ${ARM_NONE_EABI_GCC_VERSION})"
            )
        endif()


        message(
            STATUS 
            "Found arm-none-eabi-gcc: ${ARM_NONE_EABI_GCC_EXECUTABLE} "
            "(found version \"${ARM_NONE_EABI_GCC_VERSION}\", required \"${VERSION}\")")
        set(ARM_NONE_EABI_GCC_FOUND TRUE PARENT_SCOPE)
    else()
        message(DEBUG "arm-none-eabi-gcc not found")
        set(ARM_NONE_EABI_GCC_FOUND FALSE PARENT_SCOPE)
    endif()
endfunction()

function(find_arm_none_eabi_gcc_components)
    set(OPTIONS NO_SYSTEM_PATH)
    set(ONE_VALUE)
    set(MULTI_VALUE)
    cmake_parse_arguments(
        PARSE_ARGV 0 
        FIND_ARM_NONE_EABI_GCC_COMPONENTS
        "${OPTIONS}" 
        "${ONE_VALUE}" 
        "${MULTI_VALUE}"
    )

    if(FIND_ARM_NONE_EABI_GCC_COMPONENTS_NO_SYSTEM_PATH)
        set(NO_SYSTEM_PATH "NO_SYSTEM_ENVIRONMENT_PATH")
    endif()

    find_program(
        CMAKE_C_COMPILER
        NAMES arm-none-eabi-gcc
        PATHS ${ARM_NONE_EABI_GCC_ROOT} ENV{ARM_NONE_EABI_GCC_ROOT}
        PATH_SUFFIXES bin 
        REQUIRED 
        ${NO_SYSTEM_PATH}
        )

    find_program(
        CMAKE_CXX_COMPILER 
        NAMES arm-none-eabi-g++     
        PATHS ${ARM_NONE_EABI_GCC_ROOT} ENV{ARM_NONE_EABI_GCC_ROOT} 
        PATH_SUFFIXES bin 
        REQUIRED 
        ${NO_SYSTEM_PATH}
        )

    find_program(
        CMAKE_ASM_COMPILER 
        NAMES arm-none-eabi-gcc     
        PATHS ${ARM_NONE_EABI_GCC_ROOT} ENV{ARM_NONE_EABI_GCC_ROOT} 
        PATH_SUFFIXES bin 
        REQUIRED 
        ${NO_SYSTEM_PATH}
        )

    find_program(
        CMAKE_AR           
        NAMES arm-none-eabi-ar      
        PATHS ${ARM_NONE_EABI_GCC_ROOT} ENV{ARM_NONE_EABI_GCC_ROOT} 
        PATH_SUFFIXES bin 
        REQUIRED 
        ${NO_SYSTEM_PATH}
        )

    find_program(
        CMAKE_OBJCOPY      
        NAMES arm-none-eabi-objcopy 
        PATHS ${ARM_NONE_EABI_GCC_ROOT} ENV{ARM_NONE_EABI_GCC_ROOT} 
        PATH_SUFFIXES bin 
        REQUIRED 
        ${NO_SYSTEM_PATH}
        )

    find_program(
        CMAKE_OBJDUMP      
        NAMES arm-none-eabi-objdump PATHS ${ARM_NONE_EABI_GCC_ROOT} 
        ENV{ARM_NONE_EABI_GCC_ROOT} 
        PATH_SUFFIXES bin 
        REQUIRED 
        ${NO_SYSTEM_PATH}
        )

    find_program(
        CMAKE_GCOV         
        NAMES arm-none-eabi-gcov    
        PATHS ${ARM_NONE_EABI_GCC_ROOT} ENV{ARM_NONE_EABI_GCC_ROOT} 
        PATH_SUFFIXES bin 
        REQUIRED 
        ${NO_SYSTEM_PATH}
        )

    find_program(
        SIZE               
        NAMES arm-none-eabi-size    
        PATHS ${ARM_NONE_EABI_GCC_ROOT} ENV{ARM_NONE_EABI_GCC_ROOT} 
        PATH_SUFFIXES bin 
        REQUIRED 
        ${NO_SYSTEM_PATH}
        )

    find_program(
        DEBUGGER           
        NAMES arm-none-eabi-gdb     
        PATHS ${ARM_NONE_EABI_GCC_ROOT} ENV{ARM_NONE_EABI_GCC_ROOT} 
        PATH_SUFFIXES bin 
        REQUIRED 
        ${NO_SYSTEM_PATH}
        )
endfunction()
