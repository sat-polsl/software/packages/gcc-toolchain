macro(validate_selected_mcu)
    set(cortex-m0 stm32f0 stm32l0 stm32g0 stm32wl55m0)
    set(cortex-m3 stm32f1 stm32f2 stm32l1)
    set(cortex-m4 stm32f3 stm32f4 stm32l4 lpc54s018 stm32wl55m4)
    set(cortex-m7 stm32h7 stm32f7)
    set(MCU_CORES cortex-m0 cortex-m3 cortex-m4 cortex-m7)
    foreach (MCU_CORE IN LISTS MCU_CORES)
        list(APPEND SUPPORTED_MCUS ${${MCU_CORE}})
    endforeach ()

    set(MCU "" CACHE STRING "Target MCU of the project")
    if (NOT MCU IN_LIST SUPPORTED_MCUS)
        message(FATAL_ERROR "Specified MCU is not supported.\n"
            "To properly run this project use cmake -D MCU=<mcu>.\n"
            "Currently these ones are supported: ${SUPPORTED_MCUS}")
    endif ()

    set(FLOAT_ABI soft CACHE STRING "Floating point ABI")
    foreach (MCU_CORE IN LISTS MCU_CORES)
        if (MCU IN_LIST ${MCU_CORE})
            set(TARGET_CORE ${MCU_CORE} CACHE STRING "Target MCU core")
	    if (${MCU_CORE} STREQUAL cortex-m4 OR ${MCU_CORE} STREQUAL cortex-m7 AND NOT ${MCU} STREQUAL stm32wl55m4)
                set(FLOAT_ABI hard CACHE STRING "Floating point ABI" FORCE)
                if (${MCU_CORE} STREQUAL cortex-m4)
                    set(FPU -mfpu=fpv4-sp-d16 CACHE STRING "Floating point hardware flag")
                elseif (${MCU} STREQUAL stm32f7)
                    set(FPU -mfpu=fpv5-sp-d16 CACHE STRING "Floating point hardware flag")
                elseif (${MCU} STREQUAL stm32h7)
                    set(FPU -mfpu=fpv5-d16 CACHE STRING "Floating point hardware flag")
                endif ()
            endif ()
            break()
        endif()
    endforeach()
endmacro(validate_selected_mcu)

macro(fix_cmake_defaults)
    # Fix harmful CMake defaults
    # Fix pthreads linkage on MacOS
    IF (APPLE)
        set(CMAKE_THREAD_LIBS_INIT "-lpthread")
        set(CMAKE_HAVE_THREADS_LIBRARY 1)
        set(CMAKE_USE_WIN32_THREADS_INIT 0)
        set(CMAKE_USE_PTHREADS_INIT 1)
        set(THREADS_PREFER_PTHREAD_FLAG ON)
    ENDIF ()
endmacro(fix_cmake_defaults)

macro(arm_none_eabi_gcc_setup)
    if(NOT ARM_NONE_EABI_GCC_SETUP_DONE)
        validate_selected_mcu()
        fix_cmake_defaults()

        set(CMAKE_SYSTEM_NAME embedded)
        set(CMAKE_CROSSCOMPILING 1)
        set(CMAKE_SYSTEM_PROCESSOR ARM)
        set(CMAKE_PROCESSOR_TYPE arm)
        set(CMAKE_TRY_COMPILE_TARGET_TYPE STATIC_LIBRARY)
        
        set(ARCH_FLAGS -mthumb -mcpu=${TARGET_CORE} -mfloat-abi=${FLOAT_ABI} ${FPU})
        
        add_compile_options(
            ${ARCH_FLAGS} 
            -fno-common # Prevent globals from being put in common ram
            -ffunction-sections # Generate separate ELF section for each function
            -fdata-sections # Enable elf section per variable
            -MD # Create dependencies file
        
            $<$<COMPILE_LANGUAGE:CXX>:-fno-rtti>
            $<$<COMPILE_LANGUAGE:CXX>:-fno-exceptions>
        
            $<$<CONFIG:Release>:-O${ARM_NONE_EABI_GCC_RELEASE_OPTIMIZATION}>
            $<$<CONFIG:Debug>:-O${ARM_NONE_EABI_GCC_DEBUG_OPTIMIZATION}>
        
            -Wall
            -Werror
            -Wno-psabi
        
            $<$<BOOL:${ENABLE_COVERAGE}>:--coverage>
        )
        
        add_link_options(
            ${ARCH_FLAGS}
            -static # Use only static libraries
            -nostartfiles # Don't use standard library for startup
            LINKER:--gc-sections # Delete unused code
            $<$<BOOL:${ENABLE_COVERAGE}>:--coverage>
        )
        
        add_compile_definitions(
            $<$<CONFIG:Debug>:DEBUG>
        )
        
        set(CMAKE_CXX_LINK_EXECUTABLE 
            "<CMAKE_CXX_COMPILER> <FLAGS> <CMAKE_CXX_LINK_FLAGS> <LINK_FLAGS> -o <TARGET> -Wl,--start-group <LINK_LIBRARIES> <OBJECTS> -Wl,--end-group"
        ) 
        set(ARM_NONE_EABI_GCC_SETUP_DONE TRUE)
    endif()
endmacro()

