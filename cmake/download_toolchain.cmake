set(TOOLCHAIN_WINDOWS_URL https://github.com/xpack-dev-tools/arm-none-eabi-gcc-xpack/releases/download/v${VERSION}-${XPACK_VERSION}/xpack-arm-none-eabi-gcc-${VERSION}-${XPACK_VERSION}-win32-x64.zip)
set(TOOLCHAIN_LINUX_URL https://github.com/xpack-dev-tools/arm-none-eabi-gcc-xpack/releases/download/v${VERSION}-${XPACK_VERSION}/xpack-arm-none-eabi-gcc-${VERSION}-${XPACK_VERSION}-linux-x64.tar.gz)
set(TOOLCHAIN_DARWIN_ARM64_URL https://github.com/xpack-dev-tools/arm-none-eabi-gcc-xpack/releases/download/v${VERSION}-${XPACK_VERSION}/xpack-arm-none-eabi-gcc-${VERSION}-${XPACK_VERSION}-darwin-arm64.tar.gz)
set(TOOLCHAIN_DARWIN_AMD64_URL https://github.com/xpack-dev-tools/arm-none-eabi-gcc-xpack/releases/download/v${VERSION}-${XPACK_VERSION}/xpack-arm-none-eabi-gcc-${VERSION}-${XPACK_VERSION}-darwin-x64.tar.gz)

set(TOOLCHAIN_WINDOWS_ARCHIVE xpack-arm-none-eabi-gcc-${VERSION}-${XPACK_VERSION}-win32-x64.zip)
set(TOOLCHAIN_LINUX_ARCHIVE xpack-arm-none-eabi-gcc-${VERSION}-${XPACK_VERSION}-linux-x64.tar.gz)
set(TOOLCHAIN_DARWIN_ARM64_ARCHIVE xpack-arm-none-eabi-gcc-${VERSION}-${XPACK_VERSION}-darwin-arm64.tar.gz)
set(TOOLCHAIN_DARWIN_AMD64_ARCHIVE xpack-arm-none-eabi-gcc-${VERSION}-${XPACK_VERSION}-darwin-x64.tar.gz)

macro(_download_for_darwin)
    execute_process(
        COMMAND uname -m
        OUTPUT_VARIABLE ARCHITECTURE
        OUTPUT_STRIP_TRAILING_WHITESPACE
    )

    if (ARCHITECTURE STREQUAL "x86_64")
        set(TOOLCHAIN_DARWIN_URL ${TOOLCHAIN_DARWIN_AMD64_URL})
        set(TOOLCHAIN_DARWIN_ARCHIVE ${TOOLCHAIN_DARWIN_AMD64_ARCHIVE})
    elseif (ARCHITECTURE STREQUAL "arm64")
        set(TOOLCHAIN_DARWIN_URL ${TOOLCHAIN_DARWIN_ARM64_URL})
        set(TOOLCHAIN_DARWIN_ARCHIVE ${TOOLCHAIN_DARWIN_ARM64_ARCHIVE})
    endif()

    message(VERBOSE "Downloading arm-none-eabi-gcc for Darwin ${ARCHITECTURE} from: ${TOOLCHAIN_DARWIN_URL}")

    file(DOWNLOAD ${TOOLCHAIN_DARWIN_URL} ${ROOT}/${TOOLCHAIN_DARWIN_ARCHIVE})
    file(ARCHIVE_EXTRACT
         INPUT ${ROOT}/${TOOLCHAIN_DARWIN_ARCHIVE}
         DESTINATION ${ROOT})
    file(REMOVE ${ROOT}/${TOOLCHAIN_DARWIN_ARCHIVE})
endmacro()

macro(_download_for_linux)
    message(VERBOSE "Downloading arm-none-eabi-gcc for Linux from: ${TOOLCHAIN_LINUX_URL}")

    file(DOWNLOAD ${TOOLCHAIN_LINUX_URL} ${ROOT}/${TOOLCHAIN_LINUX_ARCHIVE})
    file(ARCHIVE_EXTRACT 
         INPUT ${ROOT}/${TOOLCHAIN_LINUX_ARCHIVE}
         DESTINATION ${ROOT})
    file(REMOVE ${ROOT}/${TOOLCHAIN_LINUX_ARCHIVE})
endmacro()

macro(_download_for_windows)
    message(VERBOSE "Downloading arm-none-eabi-gcc for Windows from: ${TOOLCHAIN_WINDOWS_URL}")

    file(DOWNLOAD ${TOOLCHAIN_WINDOWS_URL} ${ROOT}/${TOOLCHAIN_WINDOWS_ARCHIVE})
    file(ARCHIVE_EXTRACT 
         INPUT ${ROOT}/${TOOLCHAIN_WINDOWS_ARCHIVE}
         DESTINATION ${ROOT})
    file(REMOVE ${ROOT}/${TOOLCHAIN_WINDOWS_ARCHIVE})
endmacro()

function(_calculate_hash RESULT)
    message(DEBUG "Calculating hashes")
    
    file(GLOB_RECURSE FILES LIST_DIRECTORIES FALSE "${ROOT}/**/*")

    set(HASH_LIST "")
    foreach(FILE ${FILES})
        set(HASH_VALUE "")
        file(MD5 ${FILE} HASH_VALUE)
        list(APPEND HASH_LIST ${HASH_VALUE})

        message(DEBUG "Hashed ${FILE}: ${HASH_VALUE}")
    endforeach()

    message(DEBUG "Hashing result: ${HASH_LIST}")
    set(${RESULT} ${HASH_LIST} PARENT_SCOPE)
endfunction()

macro(download_toolchain)
    if(NOT IS_DIRECTORY ${ROOT})
        file(MAKE_DIRECTORY ${ROOT})
    endif()

    if(WIN32)
        _download_for_windows()
    elseif(APPLE)
        _download_for_darwin()
    elseif(UNIX)
        _download_for_linux()
    endif()
    file(RENAME ${ROOT}/xpack-arm-none-eabi-gcc-${VERSION}-${XPACK_VERSION} ${ROOT}/arm-none-eabi-gcc)

    set(HASH "")
    _calculate_hash(HASH)

    file(WRITE ${ROOT}/.md5 "${HASH}")
endmacro()
